#!/usr/bin/python3
# This is a Terrible python coding experiment done by someone who horribly hacks together code. Please improve.
# License: Free to all with a simple request if you improve to share it with me, so I can improve.
  
# NEEDED Depends
# python-wget pip3 install wget on centos 7
# python3-flask (pip3 install flask on centos 7)
# python3-requests (pip3 install requests on centos 7)
# Using default port 5000

import wget
import zipfile
import glob
import shutil, time
import os, subprocess
import os.path
import requests
import logging
import configparser
from flask import Flask, url_for
import configparser

parser = configparser.ConfigParser()

try:
    with open('getart.cfg') as f:
        parser.readfp(f)
except IOError:
    raise MyError()


job_id=(parser.get('common', 'job_id'))
log_name=(parser.get('location','log_name'))
cache_dir=(parser.get('location','cache_dir'))
rpm_dir=(parser.get('location','rpm_dir'))
key_name=(parser.get('key','key_name'))
key_pass=(parser.get('key','key_pass'))

app = Flask(__name__)

logging.basicConfig(filename=log_name, level=logging.INFO)

@app.route('/')
def api_root():
    return """
    <p>This is a small script that does a few things. It listens for 3 things.<br/>
    <ol type="1">
    <li> A project name: the name of the korora package.
    <li> A branch: normally testing or master
    <li> A Job ID: A job with artifacts has to be ran
    </ol>

    Once these are passed to this web service it looks for Artifacts from that<br/>
    designated project. When found (not a 404, but a 302) it then downloads the<br/>
    Artifact as a zip file uncompresses them zip file and moves the RPMS to<br/>
    their prospective locations dependant on the branch designated.<br/>
    <br/>
    This action then triggers repomange to remove the old rpms in the repo<br/>
    and rerun createrepo_c to regenerate the repo info.</p>
    
    ex. curl http://hostname:5000/project/project_name/branch/ci_job_id
    """

@app.route('/project/<pname>/<branch>/<ci_job_id>')
def api_project(pname,branch,ci_job_id):

    logging.info('Downloading artifacts for '+pname+' from branch '+branch)
    url = 'https://gitlab.com/korora/'+pname+'/-/jobs/'+ci_job_id+'/artifacts/download'
    logging.info('Attempting to use url: '+url)

    test_url = requests.head(url)
    url_test = str(test_url.status_code)
    if url_test == '404':
         logging.info('Waiting 30 seconds to get artifacts from '+url)
         time.sleep(30)

    del test_url
    test_url = requests.head(url)
    del url_test
    url_test = str(test_url.status_code)

    if url_test == '404':
         logging.info('Waiting 5 minutes to try '+url+' again.')
         time.sleep(300)

    del test_url
    test_url = requests.head(url)
    del url_test
    url_test = str(test_url.status_code)

    if url_test == '404':
        return 'Error: Failed to Download and extract '+pname+' to branch '+branch+' with 404 Error'

    wget.download(url=url, out=cache_dir+'/'+pname+'-'+branch+'.zip')

    zip_ref = zipfile.ZipFile(cache_dir+'/'+pname+'-'+branch+'.zip', 'r')
    zip_ref.extractall(cache_dir)
    zip_ref.close()

    if (branch == 'master'):
        repo_branch='releases'
    else:
        repo_branch=branch

    for dirpath, dirnames, filenames in os.walk(cache_dir):
        for filename in [f for f in filenames if f.endswith(".rpm")]:
            rpm_path = (os.path.join(dirpath, filename))
            logging.info("Files " + rpm_path)
            if not rpm_path.endswith("src.rpm"):
                logging.info("Moving "+rpm_path+' to '+rpm_dir+'/'+repo_branch+'/28/x86_64/'+filename)
                shutil.move(rpm_path,rpm_dir+'/'+repo_branch+'/28/x86_64/'+filename)
            else:
                logging.info("Moving "+rpm_path+' to '+rpm_dir+'/'+repo_branch+'/28/src/'+filename)
                shutil.move(rpm_path,rpm_dir+'/'+repo_branch+'/28/src/'+filename)

    logging.info("Cleaning "+ cache_dir)
    for the_file in os.listdir(cache_dir):
        file_path = os.path.join(cache_dir, the_file)
        if os.path.isfile(file_path):
            logging.info("Removing "+ file_path)
            os.unlink(file_path)
        elif os.path.isdir(file_path):
            shutil.rmtree(file_path)
            logging.info("Removing "+ file_path)

    logging.info("Signing RPM "+ rpm_dir+'/'+repo_branch+'/28/x86_64/'+filename)
    subprocess.getoutput('rpm-sign '+key_name+' '+key_pass+' '+ rpm_dir+'/'+repo_branch+'/28/x86_64/'+filename)
    logging.info("Cleaning Repo at "+ rpm_dir+'/'+repo_branch+'/28/x86_64/')
    clean_repo = subprocess.getoutput('repomanage -o '+rpm_dir+'/'+repo_branch+'/28/x86_64/|xargs rm -f')
    logging.info("Cleaning "+clean_repo)
    rebuild_repo = subprocess.getoutput('createrepo_c '+rpm_dir+'/'+repo_branch+'/28/x86_64/')
    logging.info("Rebuilding "+rebuild_repo)
    fix_perms = subprocess.getoutput('restorecon -R '+rpm_dir+'/'+repo_branch+'/28/x86_64/')
    logging.info("Fixing Perms "+fix_perms)
    return 'Downloaded and extracted '+pname+' to '+repo_branch


if __name__ == '__main__':
    app.run(host= '0.0.0.0')